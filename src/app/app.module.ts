import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { CarouselModule, WavesModule, ButtonsModule, ModalModule } from 'angular-bootstrap-md'
import {MatCardModule} from '@angular/material/card';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';

// layouts
import { AdminComponent } from "./layouts/admin/admin.component";
import { AuthComponent } from "./layouts/auth/auth.component";
import {MatSnackBarModule} from '@angular/material/snack-bar';
// admin views
import { DashboardComponent } from "./views/admin/dashboard/dashboard.component";
import { MapsComponent } from "./views/admin/maps/maps.component";
import { SettingsComponent } from "./views/admin/settings/settings.component";
import { TablesComponent } from "./views/admin/tables/tables.component";

// auth views
import { LoginComponent } from "./views/auth/login/login.component";
import { RegisterComponent } from "./views/auth/register/register.component";

// no layouts views
import { IndexComponent } from "./views/index/index.component";
import { LandingComponent } from "./views/landing/landing.component";
import { ProfileComponent } from "./views/profile/profile.component";
import {MatDialogModule} from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
// components for views and layouts
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminNavbarComponent } from "./components/navbars/admin-navbar/admin-navbar.component";
import { AuthNavbarComponent } from "./components/navbars/auth-navbar/auth-navbar.component";
import { CardBarChartComponent } from "./components/cards/card-bar-chart/card-bar-chart.component";
import { CardLineChartComponent } from "./components/cards/card-line-chart/card-line-chart.component";
import { CardPageVisitsComponent } from "./components/cards/card-page-visits/card-page-visits.component";
import { CardProfileComponent } from "./components/cards/card-profile/card-profile.component";
import { CardSettingsComponent } from "./components/cards/card-settings/card-settings.component";
import { CardSocialTrafficComponent } from "./components/cards/card-social-traffic/card-social-traffic.component";
import { CardStatsComponent } from "./components/cards/card-stats/card-stats.component";
import { CardTableComponent } from "./components/cards/card-table/card-table.component";
import { FooterAdminComponent } from "./components/footers/footer-admin/footer-admin.component";
import { FooterComponent } from "./components/footers/footer/footer.component";
import { FooterSmallComponent } from "./components/footers/footer-small/footer-small.component";
import { HeaderStatsComponent } from "./components/headers/header-stats/header-stats.component";
import { IndexNavbarComponent } from "./components/navbars/index-navbar/index-navbar.component";
import { MapExampleComponent } from "./components/maps/map-example/map-example.component";
import { IndexDropdownComponent } from "./components/dropdowns/index-dropdown/index-dropdown.component";
import { TableDropdownComponent } from "./components/dropdowns/table-dropdown/table-dropdown.component";
import { PagesDropdownComponent } from "./components/dropdowns/pages-dropdown/pages-dropdown.component";
import { NotificationDropdownComponent } from "./components/dropdowns/notification-dropdown/notification-dropdown.component";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { UserDropdownComponent } from "./components/dropdowns/user-dropdown/user-dropdown.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RegisterCandidatComponent } from './views/auth/register-candidat/register-candidat.component';
import { DashboradRecruterComponent } from './views/admin/dashborad-recruter/dashborad-recruter.component';
import { TableRecruterComponent } from './views/admin/table-recruter/table-recruter.component';
import { BusinessComponent } from './views/admin/business/business.component';
import { NewBusinessComponent } from './views/admin/business/new-business/new-business.component';
import { JwtInterceptor } from "./views/security/_helpers/jwt.interceptor";
import { ErrorInterceptor } from "./views/security/_helpers/error.interceptor";
import { NewOpportunityComponent } from './views/admin/table-recruter/new-opportunity/new-opportunity.component';
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { DetailsOffreComponent } from './views/admin/details-offre/details-offre.component';
import { VoirPlusComponent } from './views/admin/voir-plus/voir-plus.component';
import { DialogContentComponent } from './dialog-content/dialog-content.component';
import { BsComponentRef } from "ngx-bootstrap/component-loader";
import { MatNativeDateModule } from "@angular/material/core";


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    CardBarChartComponent,
    CardLineChartComponent,
    IndexDropdownComponent,
    PagesDropdownComponent,
    TableDropdownComponent,
    NotificationDropdownComponent,
    UserDropdownComponent,
    SidebarComponent,
    FooterComponent,
    NewBusinessComponent,
    FooterSmallComponent,
    FooterAdminComponent,
    CardPageVisitsComponent,
    CardProfileComponent,
    CardSettingsComponent,
    CardSocialTrafficComponent,
    CardStatsComponent,
    CardTableComponent,
    HeaderStatsComponent,
    MapExampleComponent,
    AuthNavbarComponent,
    AdminNavbarComponent,
    IndexNavbarComponent,
    AdminComponent,
    AuthComponent,
    MapsComponent,
    SettingsComponent,
    TablesComponent,
    LoginComponent,
    RegisterComponent,
    IndexComponent,
    LandingComponent,
    ProfileComponent,
    RegisterCandidatComponent,
    DashboradRecruterComponent,
    TableRecruterComponent,
    BusinessComponent,
    NewBusinessComponent,
    NewOpportunityComponent,
    HomeComponent,
    DetailsOffreComponent,
    VoirPlusComponent,
    DialogContentComponent,
  ],
  imports: [BrowserModule,ModalModule,AppRoutingModule,MatDatepickerModule,MatNativeDateModule , CarouselModule,  ReactiveFormsModule,
    FormsModule,MatSnackBarModule,MatDialogModule,
    MatSliderModule,  MatCardModule, HttpClientModule, BrowserAnimationsModule  , MatAutocompleteModule],

  providers:[DatePipe,MatDatepickerModule,    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }],

  bootstrap: [AppComponent],
})
export class AppModule {}
