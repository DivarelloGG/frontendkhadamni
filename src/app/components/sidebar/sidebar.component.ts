import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
})
export class SidebarComponent implements OnInit {
  collapseShow = "hidden";
  profile =false;
  constructor() {}

  ngOnInit() {
    if( sessionStorage.getItem("profile") == "candidat"){
      this.profile = true ;
    }else {
      this.profile = false ;

    }
    console.log(this.profile);
    

  }
  toggleCollapseShow(classes) {
    this.collapseShow = classes;
  }
}
