import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

// layouts
import { AdminComponent } from './layouts/admin/admin.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { BusinessComponent } from './views/admin/business/business.component';
import { NewBusinessComponent } from './views/admin/business/new-business/new-business.component';

// admin views
import { DashboardComponent } from './views/admin/dashboard/dashboard.component';
import { DashboradRecruterComponent } from './views/admin/dashborad-recruter/dashborad-recruter.component';
import { DetailsOffreComponent } from './views/admin/details-offre/details-offre.component';
import { MapsComponent } from './views/admin/maps/maps.component';
import { SettingsComponent } from './views/admin/settings/settings.component';
import { NewOpportunityComponent } from './views/admin/table-recruter/new-opportunity/new-opportunity.component';
import { TableRecruterComponent } from './views/admin/table-recruter/table-recruter.component';
import { TablesComponent } from './views/admin/tables/tables.component';
import { VoirPlusComponent } from './views/admin/voir-plus/voir-plus.component';

// auth views
import { LoginComponent } from './views/auth/login/login.component';
import { RegisterCandidatComponent } from './views/auth/register-candidat/register-candidat.component';
import { RegisterComponent } from './views/auth/register/register.component';

// no layouts views
import { IndexComponent } from './views/index/index.component';
import { LandingComponent } from './views/landing/landing.component';
import { ProfileComponent } from './views/profile/profile.component';

const routes: Routes = [
 {path:'', component: HomeComponent},
  // admin views
  {
    path: 'admin',
    component: AdminComponent,
    children: [
     // { path: "dashboard", component: DashboardComponent },
    //  { path: "dashboard-recruter", component: DashboradRecruterComponent },
      { path: "settings", component: SettingsComponent },
      { path: "opportunities", component: TablesComponent },
      { path: "opportunities-recruter", component: TableRecruterComponent },
      { path: "newOpportunity", component: NewOpportunityComponent },
     // { path: "maps", component: MapsComponent },
      { path: "profile", component: ProfileComponent },
      { path: "business", component: BusinessComponent },
      { path: "newBusiness", component: NewBusinessComponent },
      { path:"more", component:VoirPlusComponent},
      { path: "", redirectTo: "auth/login", pathMatch: "full" },
    ],
  },
  // auth views
  {
    path: 'auth',
    component: AuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'register-candidat', component: RegisterCandidatComponent },
      { path: '', redirectTo: 'login', pathMatch: 'full' },
    ],
  },
  // no layout views
  { path: 'landingPageYeAnasChbikMerithech9bal', component: LandingComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
