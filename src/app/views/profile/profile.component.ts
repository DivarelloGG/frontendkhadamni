import { Component, OnInit } from "@angular/core";
import { candidat } from "../entities/candidat";
import { UserService } from "../services/userService";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
})
export class ProfileComponent implements OnInit {
  constructor(private userService : UserService) {}
  candidat : candidat = new candidat()
  ngOnInit(): void {
    this.userService.findByUsername(sessionStorage.getItem("username")).subscribe(res=>{
      this.candidat = res.candidate ;
    })
  }
}
