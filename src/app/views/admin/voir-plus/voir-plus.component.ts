import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { candidat } from '../../entities/candidat';
import { opportunity } from '../../entities/Opportunity';
import { SubmitedOffre } from '../../entities/SubmitedOffre';
import { AuthenticationService } from '../../services/authentication.service';
import { OpportunitiesService } from '../../services/opportunities.service';
import { submittedOffre } from '../../services/submittedOffre.service';

@Component({
  selector: 'app-voir-plus',
  templateUrl: './voir-plus.component.html',
  styleUrls: ['./voir-plus.component.css']
})
export class VoirPlusComponent implements OnInit {
  opportunities : opportunity[];
  desc:any
  closureDate:any
  requiredSkills:any
  preferencesSkills:any
  price:any
  recruteur:any
  title:any
  adresse:any
  opp:opportunity
  candidat:candidat
  offre =new SubmitedOffre()
  con:candidat
  constructor(private _snackBar: MatSnackBar,private authetificationService:AuthenticationService,private opportunitieService :OpportunitiesService, private submitedOffserv:submittedOffre) { }

  ngOnInit(): void {
    this.authetificationService.getuserbyusername(sessionStorage.getItem("username")).subscribe(res=>{
      this.candidat = res.candidate ;
      console.log(this.candidat,"candidat1");
      
    })
    this.opportunitieService.findOppById(localStorage.getItem('id')).subscribe(res=>{
      this.opp=res
      console.log(this.opp,"opp1");
    })
console.log(localStorage.getItem('opp'), "voilààà");
this.desc=localStorage.getItem('opp')
this.closureDate=localStorage.getItem('closuredate')
this.requiredSkills=localStorage.getItem('requiredSkills')
this.preferencesSkills=localStorage.getItem('preferencesSkills')
this.price=localStorage.getItem('price')
console.log(this.recruteur=localStorage.getItem('recruteur') , "recruuuuuuuuu");
 this.recruteur=localStorage.getItem('recruteur')
this.title=localStorage.getItem('title')
console.log(this.title, "hhhhhhhhhhhhhhhh");

console.log(this.adresse=localStorage.getItem('adresse'), "adressaa");
 
  }
  apply(){
    //let con:candidat;
    this.con=this.candidat
    this.offre.condidat= this.con
  this.offre.opportunite=this.opp
  
  
      this.submitedOffserv.create(this.offre).subscribe()

      console.log(this.candidat,"fff")
      console.log(this.offre.opportunite,"opportuniteeee")
      let snackBarRef = this._snackBar.open('Vous avez postuler dans l"offre', 'OK');
  

  }

}
