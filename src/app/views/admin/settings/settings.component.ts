import { Component, OnInit } from "@angular/core";
import { candidat } from "../../entities/candidat";
import { CandidatService } from "../../services/candidatService";
import { UserService } from "../../services/userService";

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
})
export class SettingsComponent implements OnInit {
  constructor(private candidatService : CandidatService,private userService : UserService) {}
  candidat : candidat = new candidat() ;
  ngOnInit(): void {
    this.userService.findByUsername(sessionStorage.getItem("username")).subscribe(res=>{
      console.log(res);
      this.candidat = res.candidate
      
    })

  }
  update(){
    console.log(this.candidat);
    this.candidatService.create(this.candidat).subscribe(res=>{
      this.candidat = res 
    })
    
  }
}
