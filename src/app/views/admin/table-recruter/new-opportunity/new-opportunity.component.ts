import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Business } from 'src/app/views/entities/Business';
import { opportunity } from 'src/app/views/entities/Opportunity';
import { Recruteur } from 'src/app/views/entities/Recruteur';
import { AuthenticationService } from 'src/app/views/services/authentication.service';
import { BusinessService } from 'src/app/views/services/business.service';
import { OpportunitiesService } from 'src/app/views/services/opportunities.service';
import { UserService } from 'src/app/views/services/userService';

@Component({
  selector: 'app-new-opportunity',
  templateUrl: './new-opportunity.component.html',
  styleUrls: ['./new-opportunity.component.css']
})
export class NewOpportunityComponent implements OnInit {
  opportunity  = new opportunity();
  businessmodel:any
  pricemodel:any
  descmodel:any
  dateclo:any
  skillsmodel:any
  titremodel:any 
  business : Business[] = [];
  recruter : Recruteur = new Recruteur();
  opportunité:opportunity=new opportunity();
  selectedValue : Business = new Business();
  opportunites=new Array<opportunity>()
  busniesses= new Array<Business>()
  firstFormGroup: FormGroup;
  recruteur:Recruteur
  societe:Business
  societe2:Business
  stateCtrl = new FormControl();
  filteredOptions: Observable<Business[]>;
  id: any;
  constructor(private router : Router,  private datePipe: DatePipe ,private _formBuilder: FormBuilder ,private userService : UserService,private businessService : BusinessService , private opportuniteService : OpportunitiesService,private authetificationService :AuthenticationService) { }

  ngOnInit(): void {
    this.authetificationService.getuserbyusername(sessionStorage.getItem("username")).subscribe(res=>{
      this.recruteur = res.recruteur.id ;
      console.log(this.recruteur, "recruuuuu");
      console.log(res.recruteur.id, "yallla");
      localStorage.setItem("idRecruteur",this.recruteur.id)
      this.businessService.findByidRecruter(res.recruteur.id).subscribe(res=>{ 
        this.busniesses=res
        this.filteredOptions = this.stateCtrl.valueChanges
        .pipe(
          startWith(''),
          map(value => typeof value === 'string' ? value : value.name),
          map(name => name ? this._filter(name) : this.busniesses.slice())
        );
      })
    })
    this.opportuniteService.findAll().subscribe(res=>{
      this.opportunites=res
      console.log(this.opportunites
        
        ,"opppp");
      

    })
   
  /*   console.log(sessionStorage.getItem("username"));
    this.authetificationService.getuserbyusername(sessionStorage.getItem("username")).subscribe(res=>{
      console.log(res.recruteur);
      
      this.businessService.findByidRecruter(res.recruteur.id).subscribe((res1)=>{
        console.log(res1);
        this.business = res1 ;
      })
    })
 */
    
  
     this.firstFormGroup = this._formBuilder.group({
      Title: ['', Validators.required],
      requiredSkills: ['', Validators.required],
 closureDate: ['', Validators.required],
      price:['', Validators.required],
      description: [''],
      id:null,
      business:['']
      
    }); 
    
  }

 /*  save(){
    this.opportunity.postedFor = this.selectedValue
    console.log(this.opportunity);

    console.log(this.selectedValue);
    
    this.opportuniteService.create(this.opportunity).subscribe(res=>{
      console.log(res);
      this.router.navigateByUrl("/admin/opportunities-recruter")
      
    })
     
  }*/
  changevalue(event){
    console.log(event.target.option.value,"log");
    
  }
  test(event){
    console.log(event,"eventt");
    
  }
  submit(){
   
    /* if(this.firstFormGroup.invalid ){
      alert('champ vide!! :-)\n\n' + JSON.stringify(this.firstFormGroup.value))
      //this.add=false
    }else{ */
      let date_cloture=new Date()
      let societe=new Business
      date_cloture=this.dateclo
      this.opportunity.closureDate=this.datePipe.transform(date_cloture,'yyyy-MM-dd') ; 
      this.opportunity.Title=this.titremodel
      console.log( this.opportunity.Title, "vas y")
      console.log(this.firstFormGroup.get("Title").value, "emchyyyy");
      
      this.opportunity.requiredSkills=this.skillsmodel
      this.opportunity.price=this.pricemodel
      this.opportunity.description=this.descmodel
      societe=this.busniesses.find(x=>x.id==this.businessmodel)
      console.log(societe,"sis");
      this.opportunity.postedFor=societe
      
    //  this.opportunity.busniess=this.busniesses.find(x=>x.id===this.businessmodel)
   //  this.id =this.firstFormGroup.get("business").value

  console.log(this.businessmodel, "zfzf");
  //console.log(this.firstFormGroup.get("busniess").value, "id22");
  
  

/* la relation de l'ffre avec le busniess à voir demain + l'ajout des dates*/

    // this.opportunity.busniess.id=localStorage.getItem("idRecruteur")
   //  console.log( this.opportunity.busniess.id,"iddddddddddddd1");
    //  console.log(this.recruteur.id, "id2");
    //
      
       this.opportuniteService.create(this.opportunity).subscribe(res=>{
        console.log(res, "opppp");
        this.router.navigateByUrl("/admin/opportunities-recruter")
        
      }) 
    
  }
  
  private _filter(name: string): Business[] {
    const filterValue = name.toLowerCase();
    return this.busniesses.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
}
}
