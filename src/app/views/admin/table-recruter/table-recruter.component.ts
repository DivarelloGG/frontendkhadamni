import { Component, ElementRef, OnInit,TemplateRef, ViewChild } from '@angular/core';
import { opportunity } from '../../entities/Opportunity';
import { AuthenticationService } from '../../services/authentication.service';
import { BusinessService } from '../../services/business.service';
import { OpportunitiesService } from '../../services/opportunities.service';
import { BsModalRef , BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import { LoginComponent } from '../../auth/login/login.component';
import { DialogContentComponent } from 'src/app/dialog-content/dialog-content.component';
import { submittedOffre } from '../../services/submittedOffre.service';
import { ThrowStmt } from '@angular/compiler';


@Component({
  selector: 'app-table-recruter',
  templateUrl: './table-recruter.component.html',
  styleUrls: ['./table-recruter.component.css']
})
export class TableRecruterComponent implements OnInit {
  color: any;
  //modalRef: BsModalRef;
  sub:submittedOffre[]
  dropdownPopoverShow = false;
  @ViewChild('btnDropdownRef', { static: false }) btnDropdownRef: ElementRef;
  popper = document.createElement('div');
  @ViewChild('primaryModal') public primaryModal: ModalDirective;
title:string
  opportunities : opportunity[];
  constructor(private subOffreserv:submittedOffre,private businessService : BusinessService ,private dialog:MatDialog,
    private opportuniteService : OpportunitiesService,private authetificationService :AuthenticationService) { }
  ngOnInit(): void {
    this.authetificationService.getuserbyusername(sessionStorage.getItem('username')).subscribe(res=>{
      console.log(res.recruteur);
      
      this.opportuniteService.findByidRecruter(res.recruteur.id).subscribe((res1)=>{
        console.log(res1);
        this.opportunities = res1 ; 
       
       
        
      })
    })

  }


showList(){
  console.log('ineeeeeeess');
  
}
Store(){
  console.log('Riim');

}

popUp(i:any){
  const dialogConfig = new MatDialogConfig();
  dialogConfig.disableClose=false;
  dialogConfig.autoFocus=true;
  dialogConfig.width="60%";
  this.dialog.open(DialogContentComponent,dialogConfig);
  
  this.subOffreserv.findByidopp(this.opportunities[i].id).subscribe(res=>{
    localStorage.setItem("id", this.opportunities[i].id)
    console.log(res, "hedhy heya");
   // this.sub=res
    localStorage.setItem('testObject', JSON.stringify( this.sub));
    
  })
}
/* openModal(template: TemplateRef<any>, i:any) {
  this.modalRef = this.modalService.show(template);
  
} */





  
  toggleDropdown(event) {
    event.preventDefault();
    if (this.dropdownPopoverShow) {
      this.dropdownPopoverShow = false;
      this.destroyPopper();
    } else {
      this.dropdownPopoverShow = true;
      this.createPoppper();
    }
  }
  destroyPopper() {
    this.popper.parentNode.removeChild(this.popper);
  }
  createPoppper() {
 
    this.btnDropdownRef.nativeElement.parentNode.insertBefore(
      this.popper,
      this.btnDropdownRef.nativeElement.nextSibling
    );
  }

}
