import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableRecruterComponent } from './table-recruter.component';

describe('TableRecruterComponent', () => {
  let component: TableRecruterComponent;
  let fixture: ComponentFixture<TableRecruterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableRecruterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableRecruterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
