import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Business } from 'src/app/views/entities/Business';
import { Recruteur } from 'src/app/views/entities/Recruteur';
import { BusinessService } from 'src/app/views/services/business.service';
import { UserService } from 'src/app/views/services/userService';
import { BusinessComponent } from '../business.component';


@Component({
  selector: 'app-new-business',
  templateUrl: './new-business.component.html',
  styleUrls: ['./new-business.component.css']
})
export class NewBusinessComponent implements OnInit {
  business : Business = new Business();
  recruter : Recruteur = new Recruteur();
  constructor(private businessService :BusinessService,private userService : UserService, private router : Router
    ) { }

  ngOnInit(): void {
    console.log(sessionStorage.getItem("username"));
    
    this.userService.findByUsername(sessionStorage.getItem("username")).subscribe(res=>{
      this.business.recruter = res.recruteur ;
      console.log(this.business);
      
    })
  }
  save(){
    console.log(this.business);
    this.businessService.create(this.business).subscribe(res=>{
      console.log(res);
      
    })
    this.router.navigateByUrl("/admin/business")
      
  }
  
}
