import { Component, OnInit } from '@angular/core';
import { opportunity } from '../../entities/Opportunity';
import { OpportunitiesService } from '../../services/opportunities.service';

@Component({
  selector: 'app-details-offre',
  templateUrl: './details-offre.component.html',
  styleUrls: ['./details-offre.component.css']
})
export class DetailsOffreComponent implements OnInit {
  opportunities : opportunity[];
desc:any
closureDate:any
requiredSkills:any
preferencesSkills:any
price:any
  constructor(private opportunitieService :OpportunitiesService) { }

  ngOnInit(): void {
    this.opportunitieService.findAll().subscribe(res=>{
      this.opportunities = res
    })
console.log(localStorage.getItem('opp'), "voilààà");
this.desc=localStorage.getItem('opp')
this.closureDate=localStorage.getItem('closuredate')
this.requiredSkills=localStorage.getItem('requiredSkills')
this.preferencesSkills=localStorage.getItem('preferencesSkills')
this.price=localStorage.getItem('price')
  }

}
