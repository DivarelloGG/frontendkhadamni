import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboradRecruterComponent } from './dashborad-recruter.component';

describe('DashboradRecruterComponent', () => {
  let component: DashboradRecruterComponent;
  let fixture: ComponentFixture<DashboradRecruterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboradRecruterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboradRecruterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
