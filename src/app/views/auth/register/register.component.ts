import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Recruteur } from "../../entities/Recruteur";
import { userApp } from "../../entities/userApp";
import { RecruteurService } from "../../services/recruteur.service";
import { UserService } from "../../services/userService";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
})
export class RegisterComponent implements OnInit {
  fname;
    lname ;
    login ;
    password ; 
    email ;
    numTel1 ;
    recruter : Recruteur = new Recruteur()
    user : userApp = new userApp();
  constructor(private userService:UserService,private recruterService : RecruteurService,private router :Router) {}


  log(x) : void {
    console.log(x )
  }

  ngOnInit(): void {

  }
  
  register(){
    this.recruter.email = this.email ;
    this.recruter.fname = this.fname;
    this.recruter.lname = this.lname ;
    this.recruter.numTel1 = this.numTel1 ;
    this.recruter.password = this.password
   
    this.user.name = this.fname+" "+this.lname ;
    this.user.password = this.password ;
    this.user.profil = "recruter";
    this.user.username = this.email ; 
    this.user.email= this.email ;
    this.user.password_hash = this.password


   console.log(this.recruter);
   console.log(this.user);

   this.recruterService.create(this.recruter).subscribe(saved=>{

     this.user.recruteur = saved ;
   
   this.userService.create(this.user).subscribe(res=>{
     console.log(res);
     this.router.navigateByUrl("auth/login")
     
   })
 })
 }
}
