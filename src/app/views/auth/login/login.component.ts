import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { userApp } from "../../entities/userApp";
import { AuthenticationService } from "../../services/authentication.service";
import { UserService } from "../../services/userService";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnInit {
  password ; 
  userName ; 
  constructor(private authService : AuthenticationService ,private userService : UserService,  private router:Router) {}

  ngOnInit(): void {
    sessionStorage.clear()
  }

  Submit() {
    
    console.log(this.userName);
    console.log(this.password);
    let user = new userApp()
    user.username = this.userName ; 
    user.password = this.password;
    this.authService.login(user).subscribe(res=>{
      console.log(this.userName);
      
      this.userService.findByUsername(this.userName).subscribe(res=>{
        console.log(res);
        
        sessionStorage.setItem("profile",res.profil)
        sessionStorage.setItem("username",res.email);
        if( sessionStorage.getItem("profile") == "candidat"){
          this.router.navigateByUrl("/admin/opportunities")
          }
          else {
            this.router.navigateByUrl("/admin/opportunities-recruter")
    
          }
      })
     
    })
    
    

  }
}
