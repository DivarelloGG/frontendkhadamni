import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { last } from 'rxjs/operators';
import { candidat } from '../../entities/candidat';
import { userApp } from '../../entities/userApp';
import { CandidatService } from '../../services/candidatService';
import { UserService } from '../../services/userService';

@Component({
  selector: 'app-register-candidat',
  templateUrl: './register-candidat.component.html',
  styleUrls: ['./register-candidat.component.css']
})
export class RegisterCandidatComponent implements OnInit {
  firstname ;
  lastname ;
  birthdate;
  mobile;
  ville;
  password;
  email;
  candidatUser : userApp =new userApp();
  candidat: candidat = new candidat();
  constructor(private UserService : UserService,private CandidatService : CandidatService , private router : Router) { }

  log(x):void{
    console.log(x)
  }

  ngOnInit(): void {
  }
  register(){
     this.candidat.Ville = this.ville;
     this.candidat.email = this.email ;
     this.candidat.fname = this.firstname;
     this.candidat.lname = this.lastname ;
     this.candidat.numTel1 = this.mobile ;
     this.candidat.dateNaissance = this.birthdate ;
     this.candidat.password = this.password
    
     this.candidatUser.name = this.firstname+" "+this.lastname ;
     this.candidatUser.password = this.password ;
     this.candidatUser.profil = "candidat";
     this.candidatUser.username = this.email ; 
     this.candidatUser.email= this.email ;
     this.candidatUser.password_hash = this.password


    console.log(this.candidat);
    console.log(this.candidatUser);

    this.CandidatService.create(this.candidat).subscribe(saved=>{

      this.candidatUser.candidate = saved ;
    
    this.UserService.create(this.candidatUser).subscribe(res=>{
      
      console.log(res);
      this.router.navigateByUrl("auth/login")
      
    })
  })
  }
}
