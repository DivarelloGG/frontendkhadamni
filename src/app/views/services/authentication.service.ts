﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
 
//import { environment } from '@environments/environment';
import { environment } from 'src/environments/environment';
import { userApp } from '../entities/userApp';
 
@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    URL="http://localhost:8080" 
    URL2="http://localhost:8080/User/" 
    private currentUserSubject: BehaviorSubject<userApp>;
    public currentUser: Observable<userApp>;
    UserConnected :BehaviorSubject<userApp>=new BehaviorSubject<userApp>(null) ;
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<userApp>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }
 
    public get currentUserValue(): userApp {
        return this.currentUserSubject.value;
    }
 
    login(userApp) {
        return this.http.post<any>(this.URL+"/authenticate", userApp)
            .pipe(map(userApp => {
                // store userApp details and jwt token in local storage to keep userApp logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(userApp));
                this.currentUserSubject.next(userApp);
                return userApp;
            }));
    }
 
    logout() {
        // remove userApp from local storage to log userApp out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
 
    getuserbyusername(username){
return this.http.get<userApp>(this.URL2+"findbyUsername/"+username)
    }
 
    getuserByID(id){
        return this.http.get<userApp>(this.URL2+"getuserByID/"+id)
    }
}