import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Business } from '../entities/Business';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  constructor(private http:HttpClient) { }
  
  URL ="http://localhost:8080/Business" ; 

  create(business){  
    console.log(sessionStorage.getItem('token') );

       
    return this.http.post<Business>(this.URL+'/create/',business)
  }
  findByidRecruter(id){       
    return this.http.get<Array<Business>>(this.URL+'/findBusinessByIdRecruter/'+id)
  }
  
}
