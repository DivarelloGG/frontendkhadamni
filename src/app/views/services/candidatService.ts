import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { candidat } from "../entities/candidat";

@Injectable({
    providedIn: 'root'
  })

  export class CandidatService {
    constructor(private http:HttpClient) { }
  
    URL ="http://localhost:8080/Candidat" ; 

    create(candidat){     
      return this.http.post<candidat>(this.URL+'/create/',candidat)  ; 
    }

}