import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { promise } from 'selenium-webdriver';
import { opportunity } from '../entities/Opportunity';
import { SubmitedOffre } from '../entities/SubmitedOffre';

@Injectable({
  providedIn: 'root'
})
export class submittedOffre{
  
  

  URL ="http://localhost:8080/submited" ; 
  id: any;

  constructor(private http:HttpClient) { }
  

    create(submitted:SubmitedOffre){     
      return this.http.post(this.URL+'/createSubmittedCandidat',submitted)  ; 
    }
    findByidopp(id: any) {
      return this.http.get<Array<SubmitedOffre>>(this.URL+'/findByIdOpp/'+id)
    }
    
}
