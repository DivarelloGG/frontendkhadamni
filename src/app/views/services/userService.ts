import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { userApp } from "../entities/userApp";

@Injectable({
    providedIn: 'root'
  })

  export class UserService {
    constructor(private http:HttpClient) { }
  
    URL ="http://localhost:8080/User" ; 

    create(user){     
      return this.http.post<userApp>(this.URL+'/create/',user)  ; 
    }

    findByUsername(username){     
      return this.http.get<userApp>(this.URL+'/findbyUsername/'+username)
    }
}