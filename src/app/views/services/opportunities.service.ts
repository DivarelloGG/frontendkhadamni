import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { opportunity } from '../entities/Opportunity';

@Injectable({
  providedIn: 'root'
})
export class OpportunitiesService {
  
  

  URL ="http://localhost:8080/Opportunities" ; 

  constructor(private http:HttpClient) { }
  

    create(opportunity){     
      return this.http.post<opportunity>(this.URL+'/create/',opportunity)  ; 
    }
    findByidRecruter(id: any) {
      return this.http.get<Array<opportunity>>(this.URL+'/findByRecruter/'+id)
    }
    findAll() {
      return this.http.get<Array<opportunity>>(this.URL+'/findAll')

    }
    findOppById(id:any){
      return this.http.get<opportunity>(this.URL+'/findByid/'+id)
    }
}
