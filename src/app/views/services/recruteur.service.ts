import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Recruteur } from '../entities/Recruteur';

@Injectable({
  providedIn: 'root'
})
export class RecruteurService {
  URL ="http://localhost:8080/Recruteur" ; 

  constructor(private http:HttpClient) { }
  

    create(recruter){     
      return this.http.post<Recruteur>(this.URL+'/create/',recruter)  ; 
    }
}
