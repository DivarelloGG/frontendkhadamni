import { candidat } from "./candidat";
import { Recruteur } from "./Recruteur";

export class userApp {

     id;
	 username;
     token;
	 name;
	 password;
	 password_hash ;
	 email;
	 profil;
	 candidate : candidat ;
	 recruteur : Recruteur ;
}