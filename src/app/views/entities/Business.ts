import { Recruteur } from "./Recruteur";

export class Business {
 id ; 
 name ;
 adresse ;
 gouvernourat ;
 Ville ; 
 recruter : Recruteur ;
 postCode ;
 chiffreAffaire ;
 latitude ;
 longtitude;
 domainActivite ;
 nombreEmploye ; 
 logoPath ;
  mobile ; 
  tel ;
  anneeCreation ;
}